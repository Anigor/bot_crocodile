from django.contrib import admin
from .models import User, UserMessage, StatusGame


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('idd', 'count_message', 'count_play_game', 'count_play_game')


@admin.register(UserMessage)
class UserMessageAdmin(admin.ModelAdmin):
    list_display = ('user', 'send_user', 'messages')


@admin.register(StatusGame)
class StatusGameAdmin(admin.ModelAdmin):
    list_display = ('user', 'status')
