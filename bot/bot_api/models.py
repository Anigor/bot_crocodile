from django.db import models
from djchoices import DjangoChoices, ChoiceItem


class User(models.Model):

    class Meta:
        verbose_name = "Пользователь бота"
        verbose_name_plural = "Пользователи бота"

    idd = models.IntegerField("Идентификатор пользователя")
    count_message = models.IntegerField("Отправленно сообщений", default=0)
    count_play_game = models.IntegerField("Сыгранно игр", default=0)

    def __str__(self):
        return str(self.idd)


class StatusGame(models.Model):

    class Meta:
        verbose_name = "Статус игры пользователя"
        verbose_name_plural = "Статус игры пользователей"

    class Status(DjangoChoices):
        start = ChoiceItem("0", label="Старт игры")
        question = ChoiceItem("1", label="Ответ на первый вопрос")
        finish = ChoiceItem("2", label="Конец игры")

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь бота")
    status = models.CharField("Статус игры", max_length=1, choices=Status.choices, default=Status.start)


class UserMessage(models.Model):

    class Meta:
        verbose_name = "История перериски пользователя"
        verbose_name_plural = "История перериски пользователей"

    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Пользователь бота")
    send_user = models.BooleanField("Сообщение пользователя", default=True)
    messages = models.TextField("Сообщение пользователя")
