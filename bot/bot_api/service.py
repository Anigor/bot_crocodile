import difflib
from .models import User, UserMessage, StatusGame

message_bot = {
    "start": "Привет! Я могу отличить кота от хлеба! Объект перед тобой квадратный?",
    "question": "У него есть уши?",
    "cat": "Это кот, а не хлеб! Не ешь его",
    "dog": "Это хлеб, а не кот! Ешь его!",
    "error": "Для начала работы введите /start",
}


def add_message_in_history(user: User, user_message: str, bot_message: str):
    UserMessage.objects.create(user=user, send_user=True, messages=user_message)
    UserMessage.objects.create(user=user, send_user=False, messages=bot_message)

    user.count_message += 1
    user.save()


def save_status(status:StatusGame, item: StatusGame.Status):
    status.status = item
    status.save()


def comparison(message: str, words: [str]):
    max = 0
    for word in words:
        seq = difflib.SequenceMatcher(a=message, b=word)
        if max < seq.ratio():
            max = seq.ratio()
    return max


def message_positive_or_negative_form(message: str):
    positive = comparison(message, ['да', 'конечно', 'ага', 'пожалуй'])
    negative = comparison(message, ['нет', 'нет, конечно', 'ноуп', 'найн'])
    if positive > negative:
        return True
    return False


def cheak_post_message(post_data):
    user_id = post_data.get('user_id')
    message = post_data.get('message')
    if not user_id:
        return {'error': 'Не указан параметр user_id'}
    if not message:
        return {'error': 'Не указан параметр message'}

    message = ''.join(message.split()).lower()

    user, created = User.objects.get_or_create(idd=user_id)
    status = StatusGame.objects.filter(user=user).first()

    # Инициализация диалога
    if message == '/start':
        if status is None:
            StatusGame.objects.create(user=user, status=StatusGame.Status.start)
        else:
            save_status(status, StatusGame.Status.start)
        user.count_play_game += 1
        add_message_in_history(user, message, message_bot['start'])
        return {"message": message_bot['start']}

    if status is not None:
        # Ответ на первый вопрос
        if status.status == StatusGame.Status.start:
            print(message_positive_or_negative_form(message))
            if message_positive_or_negative_form(message):
                save_status(status, StatusGame.Status.question)
                add_message_in_history(user, message, message_bot['question'])
                return {"message": message_bot['question']}
            else:
                save_status(status, StatusGame.Status.finish)
                add_message_in_history(user, message, message_bot['cat'])
                return {"message": message_bot['cat']}

        # Ответ на второй вопрос
        if status.status == StatusGame.Status.question:
            if message_positive_or_negative_form(message):
                save_status(status, StatusGame.Status.finish)
                add_message_in_history(user, message, message_bot['cat'])
                return {"message": message_bot['cat']}
            else:
                save_status(status, StatusGame.Status.finish)
                add_message_in_history(user, message, message_bot['dog'])
                return {"message": message_bot['dog']}

    add_message_in_history(user, message, message_bot['error'])
    return {"message": message_bot['error']}
