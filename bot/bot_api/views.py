from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .service import cheak_post_message


class MessageView(APIView):

    def post(self, request):
        return Response(status=status.HTTP_200_OK, data=cheak_post_message(request.POST))

#     def post(self, request):
#         try:
#             request.user.settings_portal = json.dumps(request.data)
#             request.user.save()
#         except Exception as e:
#             return Response(status=status.HTTP_400_BAD_REQUEST, data={'detail': str(e), 'status': 'error'})
#
#
# response_profile = {
#     "200": openapi.Response('Профиль пользователя', UserSerializer),
#     "400": openapi.Response(examples={'detail': 'Возникла ошибка.', 'status': 'error'},
#                             description="{'detail': 'Возникла ошибка.', 'status': 'error'}"),
# }